#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <sys/wait.h>  
#include <unistd.h>    
#include <string.h>  
#include <stdlib.h>
#include <iostream>

#define SHMSIZE 200
#define UNIQENUMBER 3201

void client(char* sharedMemory, int key);

int main(int argc, char* argv[])
{

    char *shm;
	key_t key;
	int pid;
	int choice;
	key = shmget(UNIQENUMBER, SHMSIZE, 0666 | IPC_CREAT);
	if ((shm = (char*)shmat(key, (void*)0, 0)) == (char*)(-1)) {
		printf("Can't attach shared memory\n");
		exit(-1);
	}
	char *s = (char *)shm;
	while (true) {
		fflush(stdin);
		std::string str;
		printf("Enter string: ");
		getline(std::cin, str);
		strcpy(s, str.c_str());
		pid = fork();
		switch (pid) {
		case 0: {
			client(shm, key);
			printf("Exit? 1/0\n");
			do {
				fflush(stdin);
				std::cin >> choice;
			} while (choice<0 || choice>1);
			if (choice == 1) {
				kill(pid, SIGTERM);
				exit(0);
			}
			kill(getpid(), SIGTERM);
			break;
		}
		case -1: {
			perror("ERROR");
			exit(0);
		}
		default: {
			wait(NULL);
			break;
		}
	  }
	}
    return 0;
}
void client(char* sharedMemory, int key) {

	key = shmget(UNIQENUMBER, SHMSIZE, 0);
	sharedMemory = (char*)shmat(key, (void*)0, 0);
	printf("Child wrote: %s\n", sharedMemory);
	shmdt(sharedMemory);

}