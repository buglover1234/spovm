#include <csignal>
#include <cstring>
#include <cstdlib>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <cstdio>
#include <list>
#include <iostream>
#include <ncurses.h>
#include <sys/ioctl.h>
#include <termios.h>

bool kbhit();

const int PROC_DELAY = 1;

using namespace std;

int main(int argc, char** argv)
{
    int reciver = 0;
    char symbol;
    pid_t pid;
    list<pid_t> pidList;

    sigset_t waitSet;
    sigemptyset(&waitSet);
    sigaddset(&waitSet, SIGUSR2);
    sigprocmask(SIG_BLOCK, &waitSet, NULL);

    initscr();
    noecho();
    cbreak();
    nodelay(stdscr, TRUE);
    printw("Hello! This is the parent process.\n");
    printw("If you want to create new process, please press '+'\n");
    printw("If you want to delete last process, please press '-'\n");
    printw("If you want to quit, please press 'q'\n"); 

    while(true)
    {
        curs_set(0);
    if(kbhit()){
        switch(getch())
        {
            case '+':
                pid = fork();
                switch(pid)
                {
                case -1:
                    cout << "Erorr while creating child process! (fork)" << endl << endl;
                    exit(EXIT_FAILURE);
                case 0:
                    //specify the location of your child.o file
                    execv("./child", argv);
                    cout << "Error while loading child process (excec)!" << endl << endl;
                    exit(127);
                default:
                    pidList.push_back(pid);
                    sleep(PROC_DELAY);
                    break;
                }
                break;

            case '-':
                if(pidList.empty())
                {
                    cout << "There are no chidren to delete!" << endl << '\r';
                }
                else
                {
                    kill(pidList.back(), SIGKILL);
                    pidList.pop_back();
                }
                break;

            case 'q':
                if(!pidList.empty())
                {
                    for(auto &childPid : pidList)
                    {
                        kill(childPid, SIGKILL);
                    }
                    pidList.clear();
                }
                return 0;

            default:
                continue;
        }
    }

        for(auto &childPid: pidList)
        {
            kill(childPid, SIGUSR1);
            sigwait(&waitSet, &reciver);
        }
    }
    endwin();
}


bool kbhit()
{
termios term;
tcgetattr(0, &term);

termios term2 = term;
term2.c_lflag &= ~ICANON;
tcsetattr(0, TCSANOW, &term2);

int byteswaiting;
ioctl(0, FIONREAD, &byteswaiting);

tcsetattr(0, TCSANOW, &term);

return byteswaiting > 0;
}